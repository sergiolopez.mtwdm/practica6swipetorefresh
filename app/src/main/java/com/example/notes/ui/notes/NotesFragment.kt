package com.example.notes.ui.notes

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.notes.R
import com.example.notes.core.Resource
import com.example.notes.data.local.AppDataBase
import com.example.notes.data.local.LocalDataSource
import com.example.notes.data.model.Note
import com.example.notes.data.remote.ApiClient
import com.example.notes.data.remote.NoteDataSource
import com.example.notes.databinding.FragmentNotesBinding
import com.example.notes.presentation.NoteViewModel
import com.example.notes.presentation.NoteViewModelFactory
import com.example.notes.repository.NoteRepository
import com.example.notes.repository.NoteRepositoryImp
import com.example.notes.ui.notes.adapters.NoteAdapter

class NotesFragment : Fragment(R.layout.fragment_notes) {

    private lateinit var binding: FragmentNotesBinding
    private lateinit var adapter: NoteAdapter

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(
            NoteRepositoryImp(
                LocalDataSource(AppDataBase.getDataBase(this.requireContext()).noteDao()),
                NoteDataSource(ApiClient.service)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("TEST", "onViewCreated NOTESFRAGMENT")

        binding = FragmentNotesBinding.bind(view)

        binding.recyclerNotes.layoutManager = GridLayoutManager(requireContext(), 2)

        binding.btnAddNote.setOnClickListener {
            val action = NotesFragmentDirections.actionNotesFragmentToNoteEditFragment()
            findNavController().navigate(action)
        }

        viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE
                    adapter = NoteAdapter(result.data.data) { note ->
                        //Toast.makeText(this.context, note.title, Toast.LENGTH_LONG)
                        onNoteClick(note)
                    }
                    binding.recyclerNotes.adapter = adapter
                    Log.d("LiveData", "${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData", "${result.exception.toString()}")
                }
            }
        })

        binding.swipeContainer.setOnRefreshListener {
            Log.d("TEST", "in setOnRefreshListener")

            viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        adapter = NoteAdapter(result.data.data) { note ->
                            //Toast.makeText(this.context, note.title, Toast.LENGTH_LONG)
                            onNoteClick(note)
                        }
                        binding.recyclerNotes.adapter = adapter
                        Log.d("LiveData", "${result.data.toString()}")
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Log.d("LiveData", "${result.exception.toString()}")
                    }
                }
            })
            //fetchDataAsync()
            binding.swipeContainer.setRefreshing(false)
        }
        // Configure the refreshing colors

        binding.swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );
    }

    fun fetchDataAsync() {
        adapter.clear()
        adapter.addAll()
    }

        private fun onNoteClick(note: Note) {
        val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment(
            note.id,
            note.title,
            note.content,
            note.image
        )
        findNavController().navigate(action)
    }
}